"""
Clases y objetos

"""
class Galleta:
	chocolate = False 

una_galleta = Galleta()
otra_galleta = Galleta()


#atributos y metodos

una_galleta.sabor = "salado"
una_galleta.color = "café"

print("el color es -> ",una_galleta.color)

#metodo init y palabra reservada Self

class Galleta():
	chocolate  = False
	def __init__(self,sabor = None,forma = None): #init es el metodo constructor que crea las intancias

		self.sabor = sabor 
		self.forma = forma
		if sabor is not None and forma is not None:
			print("se acaba de crear una galleta {} {}".format(sabor,forma))
	def chocolatear(self):
		self.chocolate = True
		print(self.chocolate)
	
	def tiene_chocolate(self):
		if self.chocolate :
			print("la galleta tiene chocolate")
		else:
			print("la galleta no tiene chocolate")
	 	 		

g = Galleta("dulce","cuadrada")
g.tiene_chocolate


#leccion 4 metodos especiales de clase
class Pelicula:
	#consturctor de la clase
	def __init__(self,titulo,duracion,lanzamiento):
		self.titulo = titulo
		self.duracion = duracion
		self.lanzamiento = lanzamiento
		print("se ha credo una pelicula ->{} de {} minutos de duración del año {}".format(self.titulo,self.duracion,self.lanzamiento))

	#destructor de una clase -> nota mental : cuando el programa termina de funcionar se borra 
	#la memoria en el computador, por esto cuando deja de correr el programa se lanza el metodo
	#destructor con el mensaje definido en el print
	#def __del__(self):

	#	print("se ha borrado la pelicula -> ",self.titulo)
	
	#redefinimos el metodo string (str)
	def __str__(self):
		return "{} lanzada en {}, con una duracion de {} minutos".format(self.titulo,self.lanzamiento,self.duracion)
    def __cmp__(self,other):
    	if self.titulo < other.titulo:
    		rst = -1
    	elif self.titulo > other.titulo:
    		rst = 1
    	else:
    		rst = 0
    	return rst
	#definimos una funcion len
	#def __len__(self):
	#	str(self.duracion)
	#	return "duración : ",self.duracion


peli = Pelicula("EL padrino",175,1972)

print(str(peli))

print(len(peli))


#Objetos dentro de objetos

class Catalogo:
	peliculas = []

	def __init__(self,peliculas =[]):
		self.peliculas = peliculas



#no puedo ordenar una lista de objetos ><!!
	#def ordenar(self):
		#aux=self.peliculas
		#aux.sort()
		#for peli in aux:
			#print(peli)
	

	def agregar(self,peli):
		self.peliculas.append(peli)

	def mostrar(self):
		print("Catalogo de Peliculas")
		for peli in self.peliculas:
			print(peli)

	def borrar(self,peli):
		print("Borrar una pelicula",peli)
		for i,peli in enumerate (self.peliculas):
			if peli in  self.peliculas:
				self.peliculas.remove(peli)
				print("se ha borrado")
				return
			else:
				print("la pelicula no está en el catalogo")





peli1 = Pelicula("EL padrino",175,1972)
peli2 = Pelicula("Star Wars",190,1979)
peli3 = Pelicula("aaaa",190,1979)

c = Catalogo([peli1,peli2,peli3])


c.agregar(Pelicula("Star wars 2",190,1981))
c.mostrar()
#c.borrar(Pelicula("EL padgfgfrino",175,1972))

c.ordenar()
#Nota mental -> al imprimir las peliculas se utiliza el formato utilizado en la funcion
# __str__(self)

#ejercicio cartas es de POO

#HERENCIA
#capacidad de una clase de heredas sus atributos a otras clases
#clases padres y clases hijas


class producto:
	def __init__(self,refencencia,tipo,nombre,pvp,descripcion,productor = None, distribuidor = None,isbn = None,autor = None):
		self.refencencia = referencia
		self.tipo = tipo
		self.nombre = nombre
		self.pvp = pvp 
		self.descripcion = descripcion
		self.productor = productor
		self.distribuidor = distribuidor
		self.isbn = isbn
		self.autor = autor

	def __str__(self):
		print("se ha creado el producto {}".format(nombre))

#jerarquia

#super clase ->producto
#clases hijas -> adorno, alimento,libro

class Producto:
	def __init__(self,referencia,nombre,pvp,descripcion):
		self.referencia = referencia
		self.nombre = nombre
		self.pvp = pvp
		self.descripcion = descripcion

	def __str__(self):
		return """
		PRODUCTO:\n
		REFERENCIA\t{}
		NOMBRE\t\t{}
		pvp\t\t{}
		DESCRIPCION\t{}""".format(self.referencia,self.nombre,self.pvp,self.descripcion) 

#adorno extiende de a clase producto
class Adorno(Producto):
	pass

class Alimento(Producto):
	productor = ""
	distribuidor = ""

	def __str__(self):
		return """
		PRODUCTO:\n
		REFERENCIA\t{}
		NOMBRE\t\t{}
		pvp\t\t{}
		DESCRIPCION\t{}
		PRODUCTOR\t{}
		DISTRIBUIDOR\t{}""".format(self.referencia,self.nombre,self.pvp,self.descripcion,self.productor,self.distribuidor) 


class Libro(Producto):
	isbn = ""
	autor = ""

	def __str__(self):
		return """
		LIBRO:\n
		REFERENCIA\t{}
		NOMBRE\t\t{}
		pvp\t\t{}
		DESCRIPCION\t{}
		ISBN\t\t{}
		AUTOR\t\t{}""".format(self.referencia,self.nombre,self.pvp,self.descripcion,self.isbn,self.autor) 





ador = Adorno(111,"Vaso",1500,"Vaso bonito")
print(ador)
ali = Alimento(112,"Aceite",990,"Aceite de litro")

ali.productor = input("ingrese el proveedor : ")
ali.distribuidor = "Distribuidor xxx"

print(ali)

li = Libro(113,"Harry Potter I",12990,"Primer libro de la saga Harry Potter")

li.isbn = "11-99-11-5"
li.autor = "J. K. Rowling"

li2 = Libro(113,"Harry Potter II",12990,"Segundo libro de la saga Harry Potter")

li2.isbn = "12-99-11-5"
li2.autor = "J. K. Rowling"

print(li)


#Herencia y Polimorfismo

productos = [ador,ali,li]
productos.append(li2)
for p in productos:
	print(p,"\n")

#gestionar subclases
#isinstance es un metodo que permite verificar si un objeto es sub clase o instancia de 
#una super clase

for i in productos:
	if(isinstance(i,Adorno)):
		print(i.referencia,i.nombre)
	elif (isinstance(i,Alimento)):
		print(i.referencia,i.nombre,i.productor)
	elif (isinstance(i,Libro)):
		print(i.referencia,i.nombre,i.isbn,i.autor)



#NOta mental -> los objetos tambien se pasan por referencia !!! (valor -> se pasa una copia)
#(referencia -> se pasa el original)

#NOTA : AL IGUAL QUE LAS COLECCIONES DE DATOS LOS OBJETOS SE PASAN POR REFERENCIA ESTO QUIERE 
#DECIR QUE AL MOMENTO DE QUERER REALIZAR UNA COPIA DE UN OBJETO DE LA FORMA
# OBJ_COPIA = OBJ_ORIGINAL Y CAMBIAR UN VALOR OBJ_COPIA.NOMBRE = "ALGO NUEVO"
#Y REALIZAMOS PRINT(OBJ_ORGINAL) EL ATRIBUTO NOMBRE DEBERÍA SER "ALGO NUEVO"

#LA FORMA DE REALIZAR UNA COPIA DE UN OBJETO ES UTILIANDO UNA LIBRERIA LLAMADA COPY
#IMPORT COPY 
#COPIA_OBJ = COPY.COPY(OBJETO_ORIGINAL)


#POLIMORFIA : PROPIEDAD DE LA HERENCIA EN QUE OBJETOS DE DISTINTAS SUBCLASES PUEDAN 
#RESPONDER A LA MISMA ACCION


def rebajar_producto(p,rebaja):#p es un objeto de la clase Producto
	""" devuelve un producto con una rebaja en porcentaje de su precio"""

	p.pvp = p.pvp - (p.pvp/100 * rebaja)
	return p


ador_rebajado = rebajar_producto(ador,50)

#las variables ador_rebajado y ador hacen referencia al mismo objeto en la memoria 
print(ador_rebajado)

print(ador)

"""

EN PYTHON TODAS LAS CLASES SON A SU VEZ SUBCLASES DE LA SUPER CLASE OBJECT
ES DECIR SON POLIMORFICAS POR DEFECTO
"""

#HERENCIA MULTIPLE

"""

en este documento está la leccion  10 Poo y 11 herencias 
"""