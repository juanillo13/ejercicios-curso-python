"""
Crear una estructura de clientes para poder agregar, mostrar y eliminar mediante funciones

"""


#en primera instancia debo crear una lista 
#en segunda instancia debo ingresar los elementos de la lista, que serán un diccionario
clientes = [
	{'nombre':'juan', 'apellido': 'espinosa hernandez','edad':'25','dni':'1111111a'},
	{'nombre':'alfonso', 'apellido': 'espinosa hernandez','edad':'28','dni':'1111111b'}
]

for i in clientes:
	print("clientes -> ",i)

#en la funcion agregar_cliente debemos definir los elementos del directorio luego pasarlos
#a una variable auxiliar de tipo directorio y luego agregarlo a la lista con el metodo .append
def agregar_cliente(lista):
	nombre = input("ingrese un nombre: ")
	apellido = input("ingrese un apellido: ")
	edad = input("ingrese edad: ")
	dni = input("ingrese dni: ")
	aux= {'nombre':nombre,'apellido':apellido,'edad':edad,'dni':dni}
	for lis in lista:
		if dni == lis['dni']:
			print("el dni ya se ha ingresado anteriormente")
			return
		else:
			lista.append(aux)

#agregar_cliente(clientes)
#print(clientes)

def mostar_cliente(lista,dni):
	for lis in lista:
		if dni == lis['dni']:
			print('{} {}'.format(lis['nombre'],lis['apellido']))
			return
	print("El cliente no ha sido encontrado :C")


mostar_cliente(clientes, '1111111c')


#para borrar un registro de la lista debemos recorrerla con un for en donde en la variable i 
#iremos guardando los indices de la lista con la funcion enumerate, despues guardamos el contendio
#de la lista en la variable lis 
#debemos preguntar si los dni coinciden y de ser así ocupamos el metodo del para eliminar, y
#eliminamos el registro del indice que le pasamos a la lista. 

#por qué es lista[i] y no lis[i]?
#porque queremos eliminar el registro de la lista original, ya que lis es una variable aux que
#utliza el for para poder funcionar !!

def borrar_cliente(lista,dni):
	for i,lis in enumerate(lista):
		if dni == lis['dni']:
			del( lista[i] )
			print(str(c), " ha sido borrado")
			return
	print("El cliente no ha sido encontrado :C ")

borrar_cliente(clientes, '1111111c')