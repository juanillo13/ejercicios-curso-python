
#MANEJO DE EXCEPCIONES -> Perminte continuar con un programa aunque exista un error

n = float(input("introduce un numero: "))
m = 4
#print formateado
print("{}/{} = {}".format(n,m,n/m))

#al crear una excepcion se deja el codigo dentro de un try

try:
	n = float(input("introduce un numero: "))
	m = 4
	#print formateado
	print("{}/{} = {}".format(n,m,n/m))
except:
	print("ha ocurrido un error! intentalo de nuevo")

#se puede ocupar un while(True) para realizar el bloque 
#hasta que sea correcto

while(True):
	try:
		n = float(input("introduce un numero: "))
		m = 4
		#print formateado
		print("{}/{} = {}".format(n,m,n/m))	
	except:
		print("ha ocurrido un error! intentalo de nuevo")
	else:
		print("todo a funcionado correctamente!")
		break #importate para romper el while
	finally:
		print("fin de la iteración!")

#MULTIPLES EXCEPCIONES ESPECIFICANDO EL TIPO DE ERROR
while (True):
	try:
		a = float(input("ingrese un valor: "))
		5/a
	except TypeError:
		print("ERROR de tipo")
	except ValueError:
		print("Se debe ingresar un numero")
	except ZeroDivisionError:
		print("no se puede dividir por 0")
	except Exception as e:
		print("tipo de error -> ",type(e).__name__)
	else:
		print("todo funcionó bien")
		break
	finally:
		print("salimos de la iteración")


#INVoCACIÓN DE EXCEPCIONES

def mi_funcion(algo=None):
	try:
		if algo is None:
			raise ValueError("Error!, no se permite un valor nulo")
	except ValueError:
		print("Error!, no se permite un valor nulo (desde la excepcion)")


mi_funcion()


