

#ejercicio sacado del libro -> aprenda a pensar como un programador python


class Carta:
	"""
	Un atributo de clase se de¯ne fuera de cualquier m¶etodo, y puede accederse
	desde cualquiera de los m¶etodos de la clase.
	En general no es una buena idea modificar los atributos de clase.
	"""
	#atributos de clase
	listaDePalos = ["trebol","diamante","corazones","picas"]
	listaDeValores = ["nada","as","2","3","4","5","6","7","8","9","10","sota","reina","rey"]
	def __init__(self,palo = 0,valor = 0):
		self.palo = palo
		self.valor= valor
	def __str__(self):
		return (self.listaDeValores[self.valor] + " de " + self.listaDePalos[self.palo])
		"""
			significa usa el atributo palo del objeto self como un indice dentro del atributo 
			de clase denominado listaDePalos, y selecciona la cadena apropiada".
	
		"""
	def __cmp__(self,otro):
		if self.palo > otro.palo:
			return 1
		elif self.palo < otro.palo:
			return -1
		elif self.valor > otro.valor:
			return 1
		elif self.valor < otro.valor:
			return -1
		else:
			return 0

class Mazo:
	def __init__(self):
		self.cartas = []
		for palo in range (4):
			for valor in range (1,14):
				self.cartas.append(Carta(palo,valor))
	def __str__(self):
		s = ""
		for i in range(len(self.cartas)):
			s = s + " "*i + str(self.cartas[i])+ "\n"
		return s

	def mostrarMazo(self):
		for carta in self.cartas:
			print (carta)

	def barajarMazo(self):
		import random
		nCartas = len(self.cartas)
		for i in range(nCartas):
			j = random.randrange(i,nCartas)
			self.cartas[i],self.cartas[j] =\
			self.cartas[j],self.cartas[i]

	def eliminarCarta(self,carta):
		print("la carta es ->",carta)
		for carta in (self.cartas):
			if carta in self.cartas:	
				self.cartas.remove(carta)
				print("se ha borrado la carta ->",carta)
				return   

			else:
				return "no se ha encontrado la carta"

	def darCarta(self):
		return self.cartas.pop()



m = Mazo()
m.barajarMazo()
#print(m)
m.mostrarMazo()
m.eliminarCarta(Carta(0,1))

print("la nueva baraja es : ")
m.mostrarMazo()


#carta1 = Carta(0,3)

#print(carta1)

#pagina 187S