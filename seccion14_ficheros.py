"""

operaciones con ficheros:
- creación
- apertura
- cierre
- extensión

nota -> siempre se debe cerrar un fichero

puntero del fichero -> 	representa el lugar donde apunta el fichero antes de realizar una operación

"""

#Ficheros de texto! 

from io import open #para poder abrir ficheros! 

texto = "Una linea con texto\notra linea con texto"
#crear un fichero
fichero = open('prueba.txt','w') #primer argumento es el nombre del fichero a crear y el segundo la operación

fichero.write(texto)

fichero.close()

#Leer un fichero

fichero = open('prueba.txt','r')
#guardo la información del fichero en una viariable para ser mostrada por pantalla
#(texto_a_leer = fichero.read()

#print(texto_a_leer)

#formas de leer las lineas del fichero
lineas_del_fichero = fichero.readlines() #me guarda las lineas del fichero en una lista

fichero.close()
print(lineas_del_fichero)


fichero = open('prueba.txt','a') # me permite agregar contenido al fichero, si le diera el permiso w se me borraria todo el fichero que tengo

fichero.write('\nnueva linea ingresada con el permiso a de append')

fichero.close()

#nota:cuando se abre un archivo con el permiso w el puntero del fichiero se encuentra al principio
#del fichero por eso sobre escribe todo el contenido del archivo, con el permiso a el puntero se encuentra
#al final por eso no sobre escribe y permite agregar mas contenido
#ademas para abrir un archivo que no existe se utiliza el permiso 'a' ya que 'r' al no existir tira error 

# Leer linea por linea

fichero = open('prueba.txt','r')

linea1 = fichero.readline()
linea2 = fichero.readline()
linea3 = fichero.readline()

fichero.close()
print(linea1)
print(linea2)
print(linea3)

#Lectura seccuencial de las lineas de un fichero

with open('prueba.txt','r') as fichero:
	print("lectura automatizada")
	for linea in fichero:
		print(linea)
fichero.close()


#manejo del puntero

fichero = open('prueba.txt','r')

fichero.seek(10) #puntero está en el caracter numero 10, si quiero imprimir parto desde donde está el puntero

fichero.seek(0)

print(fichero.read(5)) # le digo que me imprima solo 5 caracteres a la funcion read, 
#el puntero debe estar en el caracter 6


#como el puntero está en la posición 5 cuando vuelvo a llamar a la funcion read() 
#me imprime a partir de la posición 6
contenido = fichero.read()
print(contenido)

#tengo que volver a inicializar el puntero en la posición 0 con seek()

fichero.seek(0)
#si ahora imprimo el contenido, la funcion read() me mostraría todo el contenido del archivo

print(fichero.read())

#saber la longitud de una linea

b = fichero.seek(len(fichero.readline())) # me posiciona el puntero al final de la primera linea
print(b)
print(fichero.read())
fichero.seek(len(fichero.readline()))
print('n')
print(fichero.read())
fichero.close()

#lectura y escritura

fichero = open('prueba.txt','r+') #damos permisos de lectura y escritura, el puntero se posiciona en la primera posicion

fichero.write('asddfsdfdsgsfgsfa') #sobre escribe la primera linea del archivo 

fichero.close()

#sobre escribir un fichero en memoria

fichero = open('prueba.txt','r+')

lineas_fichero = fichero.readlines()

lineas_fichero[1] = "esta linea la he modificado en memoria\n"

print(lineas_fichero)

fichero.seek(0) #volver el puntero al principio del fichero

fichero.writelines(lineas_fichero)

fichero.close()


#------------------pickle-------------------#
#me permite trabajar con colecciones de datos
import pickle

lista = [1,2,3,4,5]
ficheropckl = open('lista.pckl','wb')

pickle.dump(lista,ficheropckl)

ficheropckl.close()

ficheropckl = open('lista.pckl','rb')

lista_desde_fichero = pickle.load(ficheropckl)

print(lista_desde_fichero) 

ficheropckl.seek(0) #vuelvo el puntero al principio

"""
primero crear una coleccion

escribir con pickel.dump() guardamos en el fichero

con pickle.load() guardamos el contenido en una viariable

"""


#ejemplo con clases ( objetos )

class Persona:

	def __init__(self,nombre):
		self.nombre = nombre

	def __str__(self):
		return self.nombre



nombres = ["Hector","Maria","Marta"]

personas = []

for n in nombres:
	p = Persona(n)
	personas.append(p)

print(personas)

fichero_objetos = open('personas.pckl','wb')

pickle.dump(personas,fichero_objetos)

fichero.close()


fichero_objetos = open('personas.pckl','rb') #lectura binaria

persona = pickle.load(fichero_objetos) #cargando el contenido del fichero en una variable

fichero_objetos.close()

for p in persona:
	print(p)