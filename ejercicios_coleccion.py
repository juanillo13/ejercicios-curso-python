"""

1) Utilizando todo lo que sabes sobre cadenas, listas, sus métodos internos... Transforma este texto:
un día que el viento soplaba con fuerza#mira como se mueve aquella banderola -dijo un monje#lo que se mueve es el viento -respondió otro monje#ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro
En este otro:
Un día que el viento soplaba con fuerza...
- Mira como se mueve aquella banderola -dijo un monje.
- Lo que se mueve es el viento -respondió otro monje.
- Ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro.

"""

#solucion

texto = "un día que el viento soplaba con fuerza#mira como se mueve aquella banderola -dijo un monje#lo que se mueve es el viento -respondió otro monje#ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro"


lineas = texto.split("#")
print(lineas)

#estructura enumerate
#for (indice, item) in enumerate(items):
for i, linea in enumerate (lineas):
	lineas[i] = linea.capitalize()
	if i == 0:
		lineas[i] = lineas[i] + "..."
	else:
		lineas[i] = "- " + lineas[i] + "."

print(lineas)
for linea in lineas:
	print(linea) 

"""

traza de la solucion

1) transformamos el texto en una lista con split

lineas = ["un día que el viento soplaba con fuerza","mira como se mueve aquella banderola 
-dijo un monje", "lo que se mueve es el viento -respondió otro monje", 
"ni las banderolas ni el viento, lo que se mueve son vuestras mentes -dijo el maestro"]

o sea es una lista con 4 elementos -> cada elemento es una linea del texto

lineas = [0,1,2,3]  ->posiciones

como necesito los indices ocupo la funcion enumerate


i = 0 -> voy a tener lista[0] = "un día que el viento soplaba con fuerza"
i = 1 -> voy a tener lista[1] = "mira como se mueve aquella banderola 
								-dijo un monje"
i = 2 -> voy a tener lista[2]  = "lo que se mueve es el viento -respondió otro monje"
i = 3 -> voy a tener lista[3] = "ni las banderolas ni el viento, lo que se mueve son vuestras mentes 
								-dijo el maestro"
indice = i y el valor va a ser linea[i]

lineas[i] = linea.capitalize() -> estoy guardando la primera letra de cada elemento en mayuscula


"""

"""
2) Crea una función modificar() que a partir de una lista de números realice las siguientes 
tareas sin modificar la original:
1)Borrar los elementos duplicados
2)Ordenar la lista de mayor a menor
3)Eliminar todos los números impares
4)Realizar una suma de todos los números que quedan
5)Añadir como primer elemento de la lista la suma realizada
6)Devolver la lista modificada
Finalmente, después de ejecutar la función, comprueba que la suma de todos los números a partir 
del segundo, concuerda con el primer número de la lista, tal que así:

nueva_lista = modificar(lista)
print( nueva_lista[0] == sum(nueva_lista[1:]) )
> True
Nota: La función sum(lista) devuelve una suma de los elementos de una lista
"""

#solucion

lista = [99,22,10,-1,20,17,17,20,30,99]

print(lista)
def modificar(lis):
	l = list(set(lis))#1)
	#print(l)
	l.sort(reverse=True)#2)
	for i,li in enumerate(l): #3)
		if li%2 != 0:
			del( l[i] )
	#print(l)

	suma = sum(l)#4)
	#print(suma)
	l.insert(0,suma)#5)
	#print(l)

	return l

print(modificar(lista))

nueva_lista = modificar(lista)
print(nueva_lista)
print(nueva_lista[1:])#me muestra los elementos de la lista a partir de la posicion 1
print( nueva_lista[0] == sum(nueva_lista[1:]) ) 
