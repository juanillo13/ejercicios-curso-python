

#OPERADORES ENCADENADOS

#forma hasta ahora

log = 1 < 2 and 2 < 3
print (log)


#forma encadenada 

l = 1 < 2 < 3 # no es necesario el and, python lo interpreta de la misma forma 
print(l)
# la misma forma

la = 3 > 2 > 1 

print(la)

la = 3 > 2 and 2 > 1 

print(la)


#----- forma normal ------
numero = 35

if numero >= 0  and numero <= 100:
	print("el numero {} se encuentra entre 0 y 100".format(numero))
else:
	print("el numero {} no se encuentra entre 0 y 100".format(numero))



#----- forma con operadores encadenados ------
numero = 35

if 0 <= numero <= 100: # nos permite reducir las expresiones
	print("el numero {} se encuentra entre 0 y 100".format(numero))
else:
	print("el numero {} no se encuentra entre 0 y 100".format(numero))



#COMPRENSION DE LISTAS

#listas en una misma linea de codigo

#metodo tradicional 

lista = []
for letra in "casa":
	lista.append(letra)
print(lista)

#forma con comprension de lista

lista = [letra for letra in 'casa'] # en letra guardamos el elemento que estamos recorriendo por cada iteracion del bucle
print(lista)

#potencias de 2 de los primeros 10 numeros metodo tradicional

lista = []

for numero in range(0,11):
	lista.append(numero**2)
print(lista)

#metodo comprension de listas

lista = [numero**2 for numero in range(0,11)]

print(lista)

#multiplos de 2 

lista = []

for numero in range(0,11):
	if numero % 2 == 0:
		lista.append(numero)

print(lista)

#forma utilizando comprension de listas

lista = [numero for numero in range(0,11) if numero % 2 == 0]
print(lista)


#metodo tradicional

lista = []

for numero in range(0,11):
	lista.append(numero**2)
print(lista)

pares = []

for numero in lista:
	if numero % 2 == 0:
		pares.append(numero)

print (lista)
print(pares)

#forma utilizando listas comprimidas

pares = [numero for numero in [numero**2 for numero in range(0,11)] if numero % 2 ==0]
print(pares)