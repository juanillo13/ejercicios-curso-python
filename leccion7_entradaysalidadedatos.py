

#-----ENTRADA DE DATOS----#
#POR TECLADO 

#decimal= float(input("introduce un numero decimal: "))

#print(decimal)

#valores =[]

#print("introduce 3 valores")

#for x in range(3):
#	valores.append(input("introduce 3 valores: "))

#print (valores)


#----SCRIPTS----#

#los datos que se pasan al interprete se llamana argumentos
#todo lo que se envia se pasa como texto 
#para pasar mediante la terminal al script y se utiliza la libreria sys

#import sys

#print(sys.argv)


#-------SALIDAS-------#

v="otro texto"
n= 10

print("un texto",v,"un numero",n)

#FORMATEO DE ESCRITURA DE LA CADENA DE CARACTERES

#referenciando a partir de la posicion
c = "un texto {0} un numero {1}".format(v,n) #el indice representa la posicion en el formar, 0->v 1->n, se pueden cambiar

print(c)
#referenciando a partir de una clave 
d = "un texto {texto} un numero {numero}".format(texto=v,numero=n) 

print(d)


#ALIMIAMIENTO
print("{:>50}".format("palabra")) #me rellena con 50 caracteres vacios y me mueve la palabra a la derecha
print("{:50}".format("palabra")) #izquierda
print("{:^50}".format("palabra")) #centro
print("{:^50.5}".format("palabra")) #centro con truncamiento 

#FORMATEO DE NUMEROS ENTEROS RELLENANDO CON CEROS 

print("{:04d}".format(10)) #4digitos rellenando con un caracter que es 0 
print("{:04d}".format(100))
print("{:04d}".format(1000))

#FORMARTEO PARA NUMEROS DECIMALES
	#longitud	
print("{:07.2f}".format(3.123334354))
print("{:07.2f}".format(333.123334354))