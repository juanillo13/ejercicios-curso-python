

#definir las clases

class Automovil:
	automoviles = []
	def __init__(self,patente,kilometraje,revtec,marca,modelo):
		self.patente = patente
		self.kilometraje = kilometraje
		self.revtec = revtec
		self.marca = marca
		self.modelo = modelo

	def __str__(self):
		return "Automovil: {} - {} - {}".format(self.patente,self.marca,self.modelo)


class Cliente:
	clientes = []
	automoviles = []
	def __init__(self,nombre,apellido,rut,telefono,automoviles= []):
		self.nombre = nombre
		self.apellido = apellido
		self.rut = rut
		self.telefono = telefono
		self.automoviles = automoviles

	def __str__(self):
		return "Cliente: {} {} - {}".format(self.nombre,self.apellido,self.rut)



def registroCliente():
	print("----Registro de Cliente----")
	nombre = None
	while (nombre == None):
		nombre = input("Ingrese Nombre : ")
		if (len(nombre) < 10):
			print("Error! El nombre debe tener como minimo 10 letras")
			nombre = None
		else:
			print("Nombre ingresado correctamente ",nombre)

	apellido = None
	while (apellido == None):
		apellido = input("Ingrese Apellido : ")
		if (len(apellido) < 10):
			print("Error! El apellido debe tener como minimo 10 letras")
			apellido = None
		else:
			print("Apellido Ingresado Correctamente ",apellido)

	rut = None
	while (rut == None):
		rut = input("Ingrese Rut : ")
		if (len(rut) > 10) and (len(rut) < 9):
			print("Rut mal ingresado")
			rut = None
		else:
			if (len(rut) == 9):
				if rut[7] == "-":
					rutaux = rut.split("-")
					rut0=rutaux[0]
					rut1 = rutaux[1]
					if rut0.isdigit() == True:
						pass
					else:
						print("rut mal ingresado Intente de nuevo1")
						rut = None

				else:
					print("rut mal ingresado Intente de nuevo2")
					rut = None
			elif (len(rut) == 10):
				if rut[8] == "-":
					rutauxx = rut.split("-")
					print(rutauxx)
					rut2=rutauxx[0]
					rut3=rutauxx[1]
					if rut2.isdigit() == True:
						pass
					else:
						print("rut mal ingresado Intente de nuevo3")
						rut=None

				else:
					print("rut mal ingresado Intente de nuevo4")
					rut = None

			else:
				print("rut mal ingresado Intente de nuevo5")
				rut = None
		print("rut ingresado Correctamente ",rut)

	telefono = None
	while (telefono == None):
		telefono = input("Ingrese teléfono : ")
		if telefono.isdigit() == True:
			if (len(telefono)>10) and (len(telefono) < 9):
				print("ERROR! teléfono mal ingresado.Intente de nuevo")
				telefono = None
			else:
				print("Teléfono Ingresado correctamente ",telefono)

		else:
			print("ERROR! teléfono mal ingresado.Intente de nuevo")
			telefono = None

	cliente = Cliente(nombre,apellido,rut,telefono,automoviles=[])
	Cliente.clientes.append(cliente)
	print(Cliente.clientes)


def mostrarCliente():
	for i,cliente in enumerate (Cliente.clientes):
		print(i+1,") ",cliente)


def registroAutomovil():
	print("----Registro de Vehículo----")
	patente = None
	while (patente == None):
		patente = input("Ingrese Patente: Ejemplo aaaa-11 ")
		if (len(patente) == 7) :
			#print("vamos bien")
			if patente[4] == "-":
				patenteaux = patente.split("-")
				#print(patenteaux)
				auxletra = patenteaux[0]
				auxnumero = patenteaux[1]

				if auxletra.isalpha() == True:
					pass#print("vamos bien")
				else:
					print("Patente Errorea")
					patente = None
				if auxnumero.isdigit() == True:
					pass#print("vamos bien")
				else:
					#print("vamos mal2")
					print("Patente erroena")
					patente = None

			else:
				#print("vamos mal")
				print("Patente erroena")
				patente = None
		else :
			print("Patente erroena")
			patente = None
	print("Patente Ingresada Correctamente")
	print(patente)

	
	km = None
	while(km == None):
		km = input("Ingrese Kilometraje: ")
		int(km)
		if (km > "0"):
			pass
		else:
			print("Debe ser mayor a 0")
			km = None

	revtec = None
	while (revtec == None):
		revtec = input("Tiene revisión técnica? rep: Si o No : ")
		revtec.lower()
		if (len(revtec)==2):
			if (revtec.isalpha() == True):
				pass
			else:
				print("Dato mal ingresado, de ser Si o No. Intente de nuevo")
		else:
			print("Dato mal ingresado, Intente de nuevo")
	
	marca = input("Ingrese Marca: ")
	modelo = input("Ingrese Modelo: ")
	automovil = Automovil(patente,km,revtec,marca,modelo)
	#automoviles = []
	Automovil.automoviles.append(automovil)

	print (Automovil.automoviles)

	print("----Lista de Clientes----\n")
	mostrarCliente()
	print("\n")
	print("seleccione Cliente. Debe ingresar su índice")
	indice = input("Ingrese indice : ")
	
	Cliente.automoviles.append(automovil)

	print (Cliente.automoviles)


#MENU
def menu():
	while True:
		print("Bienvenido a CarWash en python\ndebe seleccionar la opción deseada")
		print("1) REGISTRAR AUTMOMOVIL")
		print("2) REGISTRAR CLIENTE")
		print("3) MOSTRAR AUTOMOVIL")
		print("4) MOSTRAR CLIENTE")
		print("5) BUSCAR AUTOMOVIL")
		print("6) SALIR")
		
		
		controlador = int(input("ingrese una opcion "))
		if controlador == 1:
			if (len(Cliente.clientes) > 0):

				registroAutomovil()
			else:
				print("No existen Clientes en el sistema. Debe ingresar un cliente")
				registroCliente()
		print("\n")

		if controlador ==2:
			registroCliente()
			print("\n")

		if controlador == 4:
			mostrarCliente()
			print("\n")	

		if controlador == 5:
			break


menu()