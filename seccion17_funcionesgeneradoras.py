

lista = [numero for numero in [0,1,2,3,4,5] if numero % 2 ==0] # Compresión de listas 
print(lista)

lista = [numero for numero in range(0,6) if numero % 2 == 0]

print(lista)

#funcion generadora -> que vaya otorgando elemtentos en tiempo de ejecucion , ejm: range



def pares(n):
	for numero in range(n+1):
		if numero % 2 == 0:
			yield numero # me devuelve un objeto generador 


print(pares(10)) #me imprime una direccion en memoria 


#se recorre como una lista, me lo imprimre como una secuencia iterada de elementos

for numero in pares(10):
	print(numero)


# o de la forma acortada

li = [numero for numero in pares(10)]

print(li) # me dvuelve una lista




lista = [1,2,3,4,5]
lista_iterable = iter(lista) # iter nos permite pasar una lista a ser una lista iterable para recorrer sus elementos con next()
print( next(lista_iterable) )
print( next(lista_iterable) )
print( next(lista_iterable) )
print( next(lista_iterable) )
print( next(lista_iterable) )



cadena = "Hola"
cadena_iterable = iter(cadena)
print( next(cadena_iterable) )
print( next(cadena_iterable) )
print( next(cadena_iterable) )
print( next(cadena_iterable) )