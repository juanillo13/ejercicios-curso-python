 #

#definicion de una funcion

def saludar():
	print("Hola! esta es una funcion")

saludar()


#funcion que multiplicacion

def tabla5():
	for i in range(10):
		print("5*",i,"=",i*5)

tabla5()

#AMBITOS DE LAS FUNCIONES 

def test():
	variable = 10 # es una variable de ambito local en la funcion test

#si una variable se define fuera de una funcion se puede ocupar dentro de una funcion ejm
#la variable tiene que estar declarada antes de llamar a la funcion 
m = 10
def test2():
	print (m)

test2()

#RETORNO DE VALORES

def prueba():
	return "una cadena de texto"
	
print(prueba())

def prueba2():
	return "una cadena",20,[1,2,3]


c,n,l = prueba2()
#estoy asignado un elemento del retorno de la funcion prueba2() a una variable
#si se le agregan mas variables a asignar de los elementos que exiten en el retorno tirar ERROR

print(c) #c -> "una cadena"
print(n) #n -> 20
print(l) #l ->[1,2,3]


#ENVIO DE VALORES

def suma(a,b): #al definir se les dice parametros de una funcion
	return a+b 

numero1 = int(input("ingrese un numero: "))
numero2 = int(input("ingrese un numero: "))
resultado = suma(numero1,numero2) # al llamar se llaman argumentos

print("la suma es: ",resultado)

#se pueden definir valores por defecto a los parametros de una funcion

def resta(a = None,b = None): #le estoy asignado un valor None a los parametros para el caso de enviar la funcion vacia 
	if a == None or b == None:
		print("enviar valores a la funcion")
		return
	else:
		return a-b

#al retornar varios valores se hacen mediante una tupla -> (valor1,valor2,valor3)
diferencia = resta(numero1,numero2)
print("la resta es: ",diferencia)


#PASO POR VALOR Y PASO POR REFERENCIA 

#PASO POR VALOR -> SE CREA UNA COPIA DENTRO DE LA FUNCION DE LOS VALORES QUE ENVIAMOS A LA FUNCION 

#LAS COLECCIONES,LISTAS, DIC, SE ENVIAN POR REFERENCIA -> SE ENVIA EL DATO ORIGINAL SI SE MODIFICAN SE CAMBIA LA ORIGINAL, SE HACE REFERENCIA AL VALOR ORIGINAL


def doblar_valor(numero): #envio de valor por referencia
	numero*= 2
	return numero
n = 10
print("valor en la funcion",doblar_valor(10))
print ("valor fuera de la funcion",n)


def doblar_valores(numeros): 
	for i,n in enumerate (numeros):
		numeros[i] *= 2
	return numeros

ns = [10,20,100]

nss = doblar_valores(ns) # le estoy asignando a nss el resultado de la funcion doblar_valores(ns) y la funcion le estoy pasando la lista -> ns
print ("los valores son: ",nss)

print ("los valores fuera de la funcion son: ",ns)

#los dos print arrojan el mismo valor aunque no estoy imprimiendo la misma variable 
#esto pasa porque al pasarle por referencia la lista ns a la funcion cualquier cambio que me 
#haga la funcion en la lista dentro de la funcion tambien se harán fuera de ella 

	
#FUNCIONES CON PARAMETROS INDETERMINADOS

def indeterminados(*args): #crea una tupla, argumentos por posicion
	print (args)

indeterminados(1,"hola",[1,2,3])

def indeterminados_nombre(**kwargs): #crea un diccionario clave - valor, argumentos por nombre clave
	for kwarg in kwargs:
		print ("clave ",kwarg,"->","valor ",kwargs[kwarg])


indeterminados_nombre(n=10,c="hola", l=[1,2,3])

#combinacion de argumentos por posicion y por nombre clave

def super_funcion(*args,**kwargs):
	total=0
	for arg in args:
		total+=arg
	print("la suma de los elementos es ",total)
	for kwarg in kwargs:
		print ("clave ",kwarg," ","valor",kwargs[kwarg]) #estoy imprimiendo dentro del for 
	print(kwargs) #imprime fuera del for y me crea el diccionario con el formato {clave: valor, clave:valor}

super_funcion(1,2,3,4,30,nombre="juan",edad = "25")



#RECURSIVIDAD 

def cuenta_atras(num):
	num -=1
	if num > 0:
		print(num)
		cuenta_atras(num)
	else:
		print("finalizo!")
	print("fin de la funcion ",num)

cuenta_atras(4)


#funcion factorial de un numero 

def factorial(numero):
	print ("el valor inicial del numero es -> ", numero)
	if numero > 1:
		numero = numero * factorial(numero -1)
	print ("el valor final es -> ", numero)
	return numero

factorial_5 = factorial(5)

print ("el factorial de 5 es: ",factorial_5)



#funciones integradas

#n = int("10") ->transforma el string en un entero
#f = float("10.5") ->transforma en un float el string

st= "esto es una cadena "+ str(10)+" "+str(3.10) #con str tansformo algo en string

print(st) 

b = bin(10) #transforma en binario
h = hex(10) #transforma en hexadecimal 

#pasar de binario a enter entero = int(numerobinario,2) el dos es la base  
 
#pasar de hexadecimal a entero = int (numerohexadecimal,16) el 16 es la base 


#round(5.4) redondea hacia bajo 

#len("hola hola") cuenta la cantidad de caracteres 
#len([1,2,3]) tambien permite contar los elementos de una lista














