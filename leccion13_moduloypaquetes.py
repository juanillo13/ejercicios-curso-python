#los modulos se importan con la palabra reservada import
#luego de importar un modulo (o libreria) de funciones debe hacer tratamiento de estos
#como si fueran métodos de una clase -> nombremodulo.metododelmodulo()


#import ejemplodemodulo

#ejemplodemodulo.saludar()

#ejemplodemodulo.sumar(10,10)

#tambien se puede importar una clase o funcion en especifico

from paquete.ejemplodemodulo import sumar

sumar(10,44)

from paquete.ejemplodemodulo import Auto


chevrollet = Auto('chevrollet','negro','2010')

chevrollet.pintar('amarillo')

#importar de golpe -> 

#from nombremodulo import * 



#PAQUETES

"""
los paquetes permiten agrupar codigo para poder ser ocupado por diferentes script
la forma de llamarlos es from paquete.modulo import * (o el nombre de la funcion o clases)
"""

from paquete.adios.despedir import *


despedida()





#Distribucion de paquetes



#modulos estándar 

"""

- copy - collections - datatime - doctest y unnittest - html,xml,json - pickle
- math - re - randorm - socket - sqlite3 - sys - threading - tkinter

"""

#collections

from collections import Counter

lista = [1,2,3,4,1,2,3,1,2,1]
#Counter devuelve un directorio con los elementos como clave y la cantidad de veces que se
#encuentran en una lista como valor {elemento: cantidad de veces}
print(Counter(lista))

p = "palabra"

print(Counter(p))

animales = "perro gato perro canario raton gato raton"

print(Counter(animales))

#para contar la cantidad de palabras que se repiten usamos split para separarlos

lista_animales = animales.split()
print(lista_animales)

print(Counter(lista_animales))
c = Counter(lista_animales)
print(c.most_common(1)) #se le pasa como argumento la cantidad de elementos a mostrar


#a usar Couter se crea un directorio es por esto que se pueden usar los metodos de los directorios

print(c.keys())
print(c.values())
print(c.items())

#se puede volver a transformar una lista

list(c)
print(c)

#diccionario de tipo por defecto

from collections import defaultdict
d = defaultdict(float)
print(d['algo'])
d1 = defaultdict(str)
d2 = defaultdict(int)
print(d1['algo'])
print(d2['algo'])


from collections import namedtuple

#tupla con nombre

Persona = namedtuple('Persona','nombre apellido edad')

p = Persona(nombre="Juan",apellido="Espinosa",edad="25")
print(p.nombre)
print(p.apellido)
print(p.edad)
print(p) #-> Persona(nombre="juan",apellido="espinosa",edad="25")

#no se pueden cambiar las tuplas, son inmutables 
#se puede acceder a los elementos 
print(p[0])


#DATATIME

import datetime

dt = datetime.datetime.now() #ahora

print(dt)

#SE PUEDE ACCESAR A DIA HORA...

print("la fecha es -> {}/{}/{} ".format(dt.day,dt.month,dt.year))

"""
se pueden acceder a los siguientes campos

dt.year
dt.month
dt.day
dt.hour
dt.minute
dt.second
dt.microsecond
dt.tzinfo -> información de la zona horaria

"""

print(dt.isoformat())

import locale

locale.setlocale(locale.LC_ALL, 'es-ES') #setear el lenguaje de python a español

hora_español = dt.strftime("%A %d de %B del %Y - %H:%M")

print(hora_español) #jueves 21 de septiembre del 2017 -  13:28

#OPERAR TIRMPO

t = datetime.timedelta(days = 14, hours = 4, seconds = 1000)

tiempo_sumado = dt + t

print(tiempo_sumado.strftime("%A %d de %B del %Y - %H:%M"))

#tambien se puede restar

tresta = dt - t

print(tresta.strftime("%A %d de %B del %Y - %H:%M"))


#-----------MODULO MATH-----------#

import math

pi = 3.14159
print(round(pi))

print(round(3.5))

#redondear a la baja

print(math.floor(pi))


#redondear al alza

print(math.ceil(pi))

#valor absoluto

print("el valor absoluto de -10 ->",abs(-10))

#sumar elementos
n = [1,2,3]
#se puede usar el metodo sum para ejemplo sumar los elementos de una lista
print(sum(n))
#se puede usar un metodo de la libreria math

print(math.fsum(n))

#truncar -> eliminar la parte decimal

print(math.trunc(100.100098987))

#potencias 

print(math.pow(2,3)) #2 elevado a 3

#raiz cuadrada
print(math.sqrt(9))

#constantes  

"""
math.pi
math.e
"""


#--------------RANDOM---------------#

import random

aleatorio = random.random() #>= 0 y < 1.0

print(aleatorio)

aleatorio2 = random.uniform(1,10) #>=1 y < 10

print(aleatorio2)

aleatorio3 = random.randrange(0,101) #numero entero aleatorio entre 0 y 100

print(aleatorio3)

#se le puede agregar un elemento mas para precisar los multiplos del numero aleatorio
#aleatorio3 = random.randrange(0,101,2) --> solo daría un multiplo de 2 

#obtener letra aleatoria de una cadena

ct = "hola mundo"

print(random.choice(ct)) #entrega solo una letra

l = [1,2,3,3,4,5,6,7]

print(random.choice(l)) #entrega un elemento de la lista aleatorio

print(random.shuffle(l))
print(l)

#obtener una muestra de un conjunto

muestra =random.sample(l,3) #primer atributo es el conjunto de muestras y el segundo es la cantidad
print(muestra)
