
#RECORDATORIO 
#SENTENCIAS DE CONTROL PUEDEN SER DE DOS TIPOS:
#CONDICIONALES (IF) : ELEGIR ENTRE DISTINTAS OPCIONES
#ITERATIVAS (FOR, WHILE) : PARA REPETIR BLOQUES DE INTRUCCIONES 

#IF (CONDICION):
	#BLOQUE DE INTRUCCIONES...CODIGO

a=5
b=10

#IF ANIDADOS	
if a==5:
	print("a vale ",a)
	if b==10:
		print("y b vale ",b)

#EN EL CASO DE QUE A NO VALIERA 5 NO ENTRA AL SEGUNDO IF YA QUE ESTA DENTRO DEL PRIMERO, ENTONCES AUNQUE 
#B VALGA 10 NO REALIZARIA EL PRINT 

if (a % 2 == 0):
	print(a," es un numero par")
else:
	print(a," no es un numero par")


comando = "salir"

if comando == "entrar":
	print("bienvenido")
elif comando == "saludar":
	print("hola hola!")
elif comando == "salir":
	print("que te vaya bonito")
else:
	print ("adios!")
# pass -> define un bloque vacio


#WHile ->a apartir de una condicion mientras sea true itera el bloque de instrucciones
c=0
while c <= 5:
	c+=1 #realiza el control del bucle 
	print("c vale ", c)


#uso de else dentro de un while
c=0 
while c <= 5:
	c+=1 #realiza el control del bucle 
	print("c vale ", c)
else:
	print ("se ha completado la iteracion y  c vale", c)


#uso del break
c=0
while c <= 5:
	c+=1 #realiza el control del bucle 
	if(c==2):
		print("rompemos el bucle usando break cuando c vale ", c)
		break
	print("c vale ", c)
else:
	print("se ha completado la iteracion y c vale",c)

#uso de continue
c=0
while c <= 5:
	c+=1 #realiza el control del bucle 
	if(c==2):
		print("usamos continue en la iteracion ", c)
		continue
	print("c vale ", c)
else:
	print("se ha completado la iteracion y c vale",c)


#ejemplo de un menu utilizando while

print("bienvenido a este menu de prueba")


#while(True):
#	print("""¿Qué quieres hacer? elige una opción
#		1) saludar
#		2) sumar dos numeros
#		3) salir""")
#	opcion = input()
#	if opcion == "1":
#		print("Hola amigo!! parezco weon programando estas cosas XD")
#	elif opcion == "2":
#		print("vamos a sumar dos numeros")
#		numero1 = float(input("ingrese el primer numero: "))
#		numero2 = float(input("ingrese el segundo numero: "))
#
#		print ("el resultado es ", numero1+numero2)
#	elif opcion == "3":
#		print ("adios! que te vaya bonito")
#		break # esta instruccion rompe el bucle cuando se elige la opcion 3
#	else:
#		print ("opcion equivocada, elige una nueva opcion") 



#SENTENCIA FOR ->crea una variable (en el caso del ejemplo es numero) y guarda la iteracion en el. La variable es una
#copia local de la lista dentro de la iteracion

#sintaxis
#FOR VARIABLE IN LISTA:
	#BLOQUE DE INSTRUCCIONES
lista = [1,2,3,4,5,6,7,8]
for numero in lista:
	print (numero)

#enumerate ->se crea una variable de indice de la forma clave-valor

for indice, numero in enumerate(lista):
	lista[indice] *= 10
	print (lista[indice]) #imprime un numero en cada iteracion

print(lista) #imprime la lista completa

cadena = "hola amigos"

for caracter in cadena:
	print (caracter)

#range permite no tener una lista la cual recorrer usando for

for i in range (3,10):
	print (i)


hola = list(range (7)) # el metodo list me genera una lista de rango (range) igual a lo que se le pasa a range
print (hola)
