import sys

#uso del paso de argumentos por la terminal 

if len(sys.argv) == 3:
	texto = sys.argv[1]
	repeticiones = int(sys.argv[2])
	for rept in range(repeticiones):
		print(texto)
else:
	print("ERROR! ingrese la cantidad de argumentos que necesita el script!")
	print("EJEMPLO -> nombre.py 'argumento' cantidad de veces ") 