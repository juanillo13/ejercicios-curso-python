
"""
Una funcion filter en conjunto con funciones lambda nos permiten filtrar sin tener que definir 
una funcion condicional preliminarmente 

filter (funcion condicion, elementos donde buscar)

donde se saca mucho valor a la funcion filter es cuando tenemos una lista de objetos
y necesitamos filtarla para obtener los objetos segun un criterio establecido

al utilizar funciones lambda y no definir la funcion condicion nos permite ahorrar memoria 
al momento de ejecutar el script



"""


numeros = [2,5,10,23,50,33]

#primero una funcion condicional, que devuelva verdadero o falso 

def multiple(numero):
	if numero % 5 == 0:
		return True

#obtenemos un objeto de tipo filter !!! importante!! se puede pasar a una lista
#al ser listas son iterables 

print(numeros)
l = list(filter(multiple,numeros))
print("---lista filtrada---")
print(l)

#utilizando funciones lambda como funcion condicion
lista = [list(filter(lambda numero : numero%5 == 0,numeros))]

print(lista)


#filtrando objetos

class Persona:

	def __init__(self,nombre,edad):
		self.nombre = nombre
		self.edad = edad

	def __str__(self):
		return "{} de {} años".format(self.nombre,self.edad)



personas = [
	Persona("juan",26),
	Persona("Marta",16),
	Persona("Pili",26),
	Persona("Focho",28),
	Persona("Eduardo",15),
	Persona("Jorge",16)
]


menores = filter(lambda persona : persona.edad < 18, personas)
mayores = filter(lambda persona : persona.edad > 18, personas)

print("---Lista completa de personas sin filtar---\n")
for persona in personas:
	print("\t",persona)

print("---Lista de personas menores de edad---\n")
for menor in menores:
	print("\t",menor)

print("---Lista de Personas mayores d edad---\n")
for mayor in mayores:
	print("\t",mayor)