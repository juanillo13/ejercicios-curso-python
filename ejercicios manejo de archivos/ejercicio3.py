from io import open
import pickle


class Personaje:
	def __init__(self,nombre,vida,ataque,defensa,alcance):
		self.nombre = nombre
		self.vida = vida
		self.ataque = ataque
		self.defensa = defensa
		self.alcance = alcance

	def __str__(self):
		return "{} de, vida: {} de ataque: {} de defensa: {} de alcance: {}".format(self.nombre,self.vida,self.ataque,self.defensa,self.alcance)

class Gestor:
	
	personajes = []	

	def __init__(self):

		self.cargar()

	def agregar(self,personaje):

		self.personajes.append(personaje)
		self.guardar()

	def borrar(self, nombre):
		for p in self.personajes:
			if p.nombre == nombre:
				self.personajes.remove(p)
				self.guardar()
				print("se ha borrado {}".format(nombre))
				return
			else:
				print("personaje no encontrado")
				return

	def mostrar(self):
		if len(self.personajes) == 0:
			print("no hay persajes")
			return
		print("------lista de personajes-----")
		for p in self.personajes:
			print(p)

	def cargar(self):
		fichero = open('personajes.pckl','ab+')
		fichero.seek(0)
		try:
			self.personajes = pickle.load(fichero)
		except:
			print("el fichero está vacío")
		finally:
			fichero.close()
			print("se han cargado {} personajes".format(len(self.personajes)))

	def guardar(self):
		fichero = open('personajes.pckl','ab+')
		pickle.dump(self.personajes,fichero)
		fichero.close()

	def __del__(self):
		self.guardar()
		print("se ha guardado el fichero")


gestor = Gestor()

gestor.agregar(Personaje('Caballero',4,2,4,2))
gestor.agregar(Personaje('Guerrero',2,4,2,4))
gestor.agregar(Personaje('Arquero',2,4,1,8))

gestor.mostrar()

gestor.borrar("Arquero")
gestor.borrar("Caballera")
gestor.mostrar()