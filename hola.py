print("hola mundo")
#frase = input()

print("""
	una linea
	otra linea
	otra linea""")

print("hola\nmundo") #\n salto de linea
print("hola\tmundo") #\t tabulacion 

print(r"C:\hola\nuevo")

#contatenacion de cadenas

cadena1= "cadena uno "
cadena2= "cadena dos"

print (cadena1+cadena2) 

palabra=input()

#slicing de una cadena de texto, esto quiere decir que de la palabra hago que me imprima solo las
#primeras 2 letras
#Slicing en las cadenas
#El slicing es una capacidad de las cadenas que devuelve un subconjunto o subcadena utilizando dos índices [inicio:fin]:
#El primer índice indica donde empieza la subcadena (se incluye el carácter).
#El segundo índice indica donde acaba la subcadena (se excluye el carácter).
#si excede el largo de la palabra se puede hacer un slicing desde [:99] y aunque no tenga 99 caracteres la palabra 
#irá desde 0 hasta cumplir toda la palabra
print(palabra[0:2])

#indices
#Los índices nos permiten posicionarnos en un carácter específico de una cadena.
#Representan un número [índice], que empezando por el 0 indica el carácter de la primera posición, y así sucesivamente.

print(palabra[1])



#listas

numeros = [0,1,2,3]
datos= [1,"hola",2] #en python puede soportar diferentes tipos de datos

#se puede usar slicing en las listas para mostrar ciertos datos dentro de la misma

print(datos[1:])

#metodo append : agrega elementos a una lista

datos.append(12)

print (datos)

#se puede hacer asignacion a una lista por slicing 

letras = ['a','b','c','d']
letras[:3] = ['A','B','C'] #intercambia las posiciones de la lista desde la 0 a la 3 con las asignadas

print(letras)
numero_de_letras = len(letras)

print(numero_de_letras)

#listas anidadas
#listas 
a = [1,2,3]
b = [4,5,6]
c = [7,8,9]

resultado = [a,b,c]

print(resultado)

#obtener un valor en una lista anidada: lista[lista anidada][elemento de la lista]
print(resultado[1][2])

#input lee cadena de caracteres por teclado, por eso hay que transformar a entero cuando sea necesario
#con el metodo int()
#para hacerlo decimal se usa float()