"""

Esta función trabaja de una forma muy similar a filter(), con la diferencia que en lugar 
de aplicar una condición a un elemento de una lista o secuencia, aplica una función sobre 
todos los elementos y como resultado se devuelve un iterable de tipo map:

en base a un conjunto de elementos le aplicamos una funcion sobre todo el conjunto

filter nos permitia afectar los elementos de un conjunto en base a una condicion

"""

numeros = [2,5,10,23,50,33]

#doblar los elementos de la lista

def doblar(numero):
	return numero * 2


m = map(doblar,numeros)

print(m) # me devuelve un objeto tipo map

m = list(m)
print(m)


#de forma analoga podemos utilizar funciones lambda

lista = list(map(lambda x: x*2,numeros)) #se pueden utlizar expresiones lambda para no utilizar bucles for

print(lista)
 #se puedes usar sobre mas de un iterable pero siempre y cuendo tengan la misma longitud

a = [1,2,3,4,5]
b = [6,7,8,9,10]

c = list(map(lambda x,y: x*y,a,b))
print(c)

d = list(map(lambda x,y,z: x*y*z,a,b,c))
print(d)


#utilizacion en objetos

class Persona:

	def __init__(self,nombre,edad):
		self.nombre = nombre
		self.edad = edad

	def __str__(self):
		return "{} de {} años".format(self.nombre,self.edad)


personas = [
	Persona("juan",26),
	Persona("Marta",16),
	Persona("Pili",26),
	Persona("Focho",28),
	Persona("Eduardo",15),
	Persona("Jorge",16)
]


#incrementar en un año la edad de las personas
#con funciones

def incremetar(persona):
	persona.edad += 1
	return persona

personas = map(incremetar,personas)

for persona in personas:
	print(persona)


#con expresiones lambda
#sobreescribir en la lista #objetos        creando en ejecucion las personas y cmabiando la edad +1
personasx = map(lambda persona: Persona(persona.nombre,persona.edad+1),personas)

for persona in personasx:
	print(persona)

