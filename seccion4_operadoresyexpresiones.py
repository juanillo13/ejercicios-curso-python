
#operadores logicos

# 3==3 - 3!= 2 - 3 > 2 - 2 < 3  -  3<=4 - 4>=3 True =1 y False = 0  

#NOT
#
variable = True
print (not(variable)) #false

#recordatorio de operadores logicos

#ejemplos de and
#FALSO AND FALSO = FALSO
#FALSO AND VERDADERO = FALSO
#VERDADERO AND FALSO = FALSO
#VERDADERO AND VERDADERO = VERDADERO

var = 25

if (var>10) and (var<20):
	print (True)
else: 
	print(False)

cadena= "hola mundo"

if (len(cadena) >= 5) and (cadena[0] == "0"):
	print (True)
else:
	print (False)

#ejemplos de OR
#False or false = false
#Falso or verdadero = verdadero
#Verdadero or falso = verdadero
#verdadero or verdadero = verdadero

c = "salir"

if (c =="salir") or (c=="fin") or (c=="exit"):
	print(True)
else:
	print(False)


#ExPRESIONES ANIDADAS

#NOTA: NORMAS DE PRESEDENCIA QUIERE DECIR EL ORDEN EN QUE SE EJECUTAN CIERTAS INSTRUCCIONES 
#primero parentesis
#segundo las expresiones aritmeticas con sus propias normas de presedencia
#tercero expresiones relacionales
#cuarto expresiones logicas


#EXPRESIONES EN ASIGNACION 

#A=1 -> ASIGNACION DE UN VALOR A UNA VARIABLE
#A += 5 -> SUMA EN ASIGNACION 
#A -= 1 ->RESTA EN ASIGNACION
#A *= 1 -> PRODUCTO EN ASIGNACION -> QUIERE DECIR QUE EL NUEVO VALOR DE LA VARIABLE ES EL CALCULADO EN LA OPERACION DE ASIGNACION
#A /= 1 ->DIVISION EN ASIGNACION
#A %= 1 ->MODULO EN ASIGNACION
#A **= ->POTENCIA EN ASIGNACION 

#EJEMPLO 

n=0

while n<10:
	if (n % 2) == 0:
		print(n,"es un numero par")
	else:
		print(n,"es un numero impar")
	n += 1
#fin del bloque while

#ejercicios

#1) 
n1= float(input("ingrese el primer numero: "))
n2= float(input("ingrese el segundo numero: "))

#mostrar si los numeros son iguales

print("¿son iguales?",n1==n2) #la expresion n1==n2 devuelve true or false

#es lo mismo que hacerlo de esta forma

ntotal = n1!= n2 
#mostrar si son distintos
print ("son distintos",ntotal)	

#se ahorran lineas de codigo al hacer la comparacion en el mismo print

print("¿es el primer numero mayor ?", n1 > n2)
print("¿es el segundo numero mayor ?", n1 < n2)


#Ejercicio 2
#2) Utilizando operadores lógicos, determina si una cadena de texto introducida por el 
#usuario tiene una longitud mayor o igual que 3 y a su vez es menor que 10 
#(es suficiene con mostrar True o False):

cadena1=input("ingrese una palabra: ")

if (len(cadena1) >= 3 ) and (len(cadena1)<=10):
	print ("la cadena cumple con las restricciones",True)
else:
	print("La cadena no cumple con las restricciones",False)