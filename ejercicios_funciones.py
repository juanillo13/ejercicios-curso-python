""" 
1) Realiza una función llamada area_rectangulo() que devuelva el área del rectangulo
 a partir de una base y una altura. Calcula el área de un rectángulo de 15 de base y 10 de altura.
Nota: El área de un rectángulo se obtiene al multiplicar la base por la altura

"""
#SOLCUION

print("Vamos a calcular la base de un triangulo")

altura = int(input("ingrese la altura del triangulo: "))
base = int(input("ingrese la base del triangulo: "))

def area_rectangulo (base,altura):

	area = base*altura
	return area 


resultado = area_rectangulo(base,altura)

print("el area del triangulo es : ",resultado)


""" 
2) Realiza una función llamada area_circulo() que devuelva el área de un círculo a partir de un radio.
 Calcula el área de un círculo de 5 de radio:
Nota: El área de un círculo se obtiene al elevar el radio a dos y multiplicando el resultado por el número
 pi. Puedes utilizar el valor 3.14159 como pi o importarlo del módulo math:

"""
#SOLCUION

import math

#print (math.pi)
print("vamos a calcular el area de un circulo")
pi = math.pi
radio = int(input("ingrese el radio del triangulo: "))

def area_circulo(pi,radio):
	area = pi*math.pow(radio,2)
	return area

result = area_circulo(pi,radio)

print("el area del circulo es -> ", result)


"""
3) Realiza una función llamada relacion() que a partir de dos números cumpla lo siguiente:
Si el primer número es mayor que el segundo, debe devolver 1.
Si el primer número es menor que el segundo, debe devolver -1.
Si ambos números son iguales, debe devolver un 0.
Comprueba la relación entre los números: '5 y 10', '10 y 5' y '5 y 5'
"""

#SOLUCION


def relacion(numero1,numero2):

	if numero1 > numero2:
		return 1
		
	elif numero1 < numero2:
		return -1
		
	elif numero1 == numero2:
		return 0
		

print(relacion(5,10))
print(relacion(10,5))
print(relacion(5,5))

"""

4) Realiza una función llamada intermedio() que a partir de dos números, devuelva su punto intermedio:
Nota: El número intermedio de dos números corresponde a la suma de los dos números dividida entre 2
Comprueba el punto intermedio entre -12 y 24
"""

#SOLUCION

def intermedio(numero1,numero2):
	resultado = (numero1 + numero2) / 2
	return resultado

resultado = intermedio(-12,24)

print ("el resultado es -> ",resultado)


"""

5) Realiza una función llamada recortar() que reciba tres parámetros. El primero es el número a
 recortar, el segundo es el límite inferior y el tercero el límite superior. La función tendrá que 
 cumplir lo siguiente:
Devolver el límite inferior si el número es menor que éste
Devolver el límite superior si el número es mayor que éste.
Devolver el número sin cambios si no se supera ningún límite.
Comprueba el resultado de recortar 15 entre los límites 0 y 10
"""

#SOLUCION
#parametro1 ->numero a recortar
#parametro2 -> limite inferior
#parametro3 -> limite superior
def recortar(parametro1,parametro2,parametro3):

	if parametro1 < parametro2:
		return parametro2
	elif parametro1 > parametro3:
		return parametro3
	else:
		return parametro1

print (recortar(15,0,10))


"""
6) Realiza una función separar() que tome una lista de números enteros y devuelva dos listas ordenadas. 
La primera con los números pares, y la segunda con los números impares:
Por ejemplo:
pares, impares = separar([6,5,2,1,7])
print(pares)   # valdría [2, 6]
print(impares)  # valdría [1, 5, 7]
Nota: Para ordenar una lista automáticamente puedes usar el método .sort().
"""
#SOLUCION

lista=[6,5,2,1,7]

def separar(lista):
	lista.sort() #me ordena la lista que le acabo de pasar a la funcion separar
	pares = []
	impares = []

	for n in lista:
		if n%2 == 0:
			pares.append(n)
		else:
			impares.append(n)
	return pares, impares

pares,impares = separar(lista)
print (pares)
print (impares)