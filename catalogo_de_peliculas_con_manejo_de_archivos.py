#Cargamos las librerias para trabajar con archivos

from io import open
import pickle

#Clases y metodos

class Pelicula:
	"""Contructor de la clase"""
	def __init__(self, titulo,duracion,lanzamiento):
		super(Pelicula, self).__init__()
		self.titulo = titulo
		self.duracion = duracion
		self.lanzamiento = lanzamiento
		print("se ha creado la pelicula: ",self.titulo)

	def __str__(self):
		return "{} ({})".format(self.titulo,self.lanzamiento)



class Catalogo:
	peliculas = []

	#constructor

	def __init__(self,peliculas = []):
		#self.peliculas = peliculas
		#cargar el contenido del fichero
		self.cargar()

	def agregar(self,pelicula):
		self.peliculas.append(pelicula)
		#despues de agregar hay que guardar en el fichero
		self.guardar()
	def mostrar(self):
		if len(self.peliculas) == 0:
			print("el catalogo está vacio")
			return
		print('-------Catalogo de peliculas--------')
		for p in self.peliculas:
			print(p)

	def cargar(self):
		fichero = open('catalogo.pckl','ab+') #appen binario que tiene permisos de lectura y escritura
		#me deja el puntero al final a si que lo tengo que re posicionar
		fichero.seek(0)
		#se usa una excepción porque el momento de crear el archivo estará vació y tiraría error
		try:
			self.peliculas = pickle.load(fichero)
		except:
			print("el fichero está vacio")
		finally:
			fichero.close()
			print("se han cargado {} peliculas".format(len(self.peliculas)))


	def guardar(self):
		fichero = open('catalogo.pckl','wb+') #vamos a sobreescribir
		pickle.dump(self.peliculas,fichero)
		fichero.close() #siempre hay que cerrar el fichero

	def __del__(self):
		self.guardar()
		print("se ha guardado el fichero")


#Crear un catalogo

catalogo = Catalogo()
#catalogo.mostrar()


#agregamos peliculas

catalogo.agregar(Pelicula('Star Wars',130,1970))
catalogo.agregar(Pelicula('Star Wars II',130,1975))
catalogo.agregar(Pelicula('Star Wars III',120,1979))

catalogo.mostrar()



catalogo.agregar(Pelicula('Star Wars IV',120,1979))

catalogo.mostrar()
print("nuevo catalogo")
catalogo2 = Catalogo()
catalogo2.mostrar()
