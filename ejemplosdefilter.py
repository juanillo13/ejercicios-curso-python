


#vamos a crear un array con nombres

nombres = ['juan','alfonso','micki','ruben','javier','jorge']

for nombre in nombres:
	print(nombre)


#ahora vamos a aplicar una funcion para cambiar todos las primeras letras a mayusculas


def mayuscula (lista):
	r=[]
	for nombre in nombres:
		m=""
		for j in nombre.split():
			m+= j[0].capitalize() + j[1:] + " "
		#print(m)
		r.append(m.rstrip())
	return '["' + '","'.join(r)+ '"]'  	

print(mayuscula(nombres))


#ahora vamos a crear una funcion map
print("-----Funcion map------")
mp = map(mayuscula,nombres) #mp es un objeto iterable
mp = list(mp)
print(mp)


#vamos a crear una funcion que nos arroje true si encontramos un nombre que empiece con j
#con estas funciones puedo manipular listas de strings 

def empieza (lista):
	si =[]
	no= []
	for nombre in lista:
		m = ""
		for letra in nombre.split():
			if letra[0] == 'j':
				si.append(letra)
				#return True
			else:
				no.append(letra)
				#return False
	#print(si)
	#print(no)
	return si
		
empieza(nombres)

l = list(filter(empieza,nombres))
print("---lista filtrada---")

print(l)
