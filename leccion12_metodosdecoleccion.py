# Metódos de clases

#cádenas de texto
#manipulación de mayusculas y minusculas

cadena = "hola mundo".upper() #transforma toda la cadena a mayuscula

cadena1 = "Hola Mundo".lower()#transforma toda la cadena a miniscula
cadena2 = "hola mundo".capitalize() #transforma la primera letra de la cadena en mayuscula
cadena3= "hola mi mundo".title()# transforma la primera letra de cada palabra en mayuscula
cadena4= "hola mundo".count('o') #permite contar la cantidad de veces que se repite una letra,devuelve un int
#pueden ser palabras completas .count('mundo')

indicedeunapalabra = "hola mundo".find('mundo')#entrega el indice donde empieza la palbra a buscar
indicedeunapalabra2 = "hola mundo mundo mundo".rfind('mundo') #entrega el indice de la ultima palabra

c = "199" #->true c = "q110" ->false
a = "sdn001dsd"


print(a.isalnum()) #permite saber si la cadeana de texto es alfanumerica  devuelve true o false

print(c.isdigit()) #permite saber si la cadena contiene puros numeros, devuelve true o false


print (cadena)

print(cadena1)
print(cadena2)
print(cadena3)
print(cadena4)
print(indicedeunapalabra)
print(indicedeunapalabra2)


e = "    "
e.isalpha() #verifica si es un cadana alfabetica, el espacio no lo considera entonces es false
e.islower() #verifica si la cadena está en miniscula (toda la cadena) devuelve true o false
e.isupper() #verifica si la cadena está en mayuscula (toda la cadena) devuelve true o false
e.istitle()#verifica si la cadena tine las letras de cada palabra en mayuscula, devuelve t o f
print(e.isspace()) #verifica si la cadena está compuesta de puros espacios(no debe haber ninguna
#letra para que tiene True, puede ser true o false

star = "hola mundo"
print(star.startswith('hola')) #sirve para saber con que letra o palabra empieza una cadena, arroja true o false
print(star.endswith('mundo')) #sirve para saber con que letra o palabra termina una cadena, arroja true o false


split = "hola. mundo. mundo. mundo".split('.') 
print(split) #crea una lista con las palabras de la cadena

print(split[-1]) #se puede realizar slicing 

#se le puede indicar la forma de separar a split

join = ",".join("hola mundo") #separa cada letra segun lo que especifiquemos
print(join)

strip = "----hola mundo---".strip("-") #quita los caracteres que le pasemos al metodo
print(strip)

replace = "hola mundo mundo mundo".replace(' mundo','',2)
print(replace)#reemplaza el primer argumento por el segundo, el tercero es la cantidad de veces


#listas

lista = [1,2,3,4]
lista.append(6)
#lista.clear() #borra los elementos de la lista

lista2 = [5,6,7]
#lista.extend(lista2) #junta ambas listas

lis = ["hola","mundo","hola","mundo"]
lis.count("hola")#devuelve la cantidad de veces que se repite un elemento en la lista

lis.index("hola")#devuelve el indice del elemento que le pasamos al metodo index (busca el primero)

lista.insert(-1,0) #inserta en la lista un elemento en la posicion que se pasa como primer parametro al metodo,
#el segundo argumento es el elemento que queremos insertar 
#cuando no encuentra el indice lo agrega al final
print(lista)

lista.pop() #saca el ultimo elemento
#aux = lista.pop(0)#lo bueno del pop es pasarle el valor que queremos
#sacarle a la lista a una variable para seguir utilizandolo
#tambien se le puede pasar un indice de que elementos queremos sacar, asi que no solo es el ultimo

lista.remove(3) #remueve un elemento, si hay repetidos remueve el primero de la lista

lista.reverse() #da vuelta una lista 
#truco para dar vuelta una cadena
#la transfrmamos en una lista la damos vuelta y despues con join la juntamos
listacadena = list("juan espinosa hernandez")
print(listacadena)
listacadena.reverse()
print(listacadena)

cadenarevertida = "".join(listacadena)
print(cadenarevertida)


#ordenar

listadesordenada = [99,-1,200,50,1,0]

listadesordenada.sort()
print(listadesordenada)

#orden de forma inversa

listadesordenada.sort(reverse=True)
print(listadesordenada)


#conjuntos

conjunto = set()
conjunto.add(1)
conjunto.add(2)
conjunto.add(3)
conjunto.add(4)
print(conjunto)
conjunto.discard(2)
print(conjunto) 

#copiar conjunto

copia = conjunto.copy()

print(copia)

copia.clear()
print(copia)

con1 = {1,2,3}
con2 = {3,4,5}
con3 = {-1,99}
con4 = {1,2,3,4,5}

#comprobar si dos conjuntos son disjuntos 

print(con1.isdisjoint(con2)) #false, o sea que no son disjuntos porque tiene  el elemento 3 en comun

print(con1.isdisjoint(con3)) #true, no tienen ningun elemento en comun
print(con1.isdisjoint(con4)) #false, tiene los elementos 1,2,3 en comun

#comprobamos si es subconjunto 

print(con1.issubset(con3))

#comprobamos si es contenedor 

print(con4.issuperset(con1)) #true 

con1.union(con2) #{1,2,3,4,5}

con1.update(con2) # con1 = {1,2,3,4,5}

con1.difference(con2) #devuelve la diferencia entre los conjuntos en este caso {1,2}

con1.difference_update(con2) # guarda en con1 la diferencia de los conjuntos con1 = {1,2}

con1.intersection(con2) #devuelve la interseccion de los conjuntos en este caso {3}

con1.intersection_update(con2)#  guarda en con1 la interseccion de los conjuntos

con1.symmetric_difference(con2) #devuelve los elementos que no concuerdan en los conjuntos
#en este caso {1,2,3,5}


#diccionarios


colores = {'amarillo':'yellow','azul':'blue','verde':'green'}
print(colores['amarillo'])
clave =input("ingrese un color")

print(colores.get(clave,'no se encuentra')) #se e pasa la clave al metodo, si no lo encuetra tira el mensaje por defecto

#clave en diccionario, otra forma
print('amarillo'in colores)

#obtener las claves

print(colores.keys())

#obtener valores

print(colores.values())

#obtener tuplas con (clave, valor)

print(colores.items())

#recordatorio
#recorrer un diccionario
for color in colores:
	print (color) #me devuelve las claves

for color in colores:
	print(color,": ",colores[color]) # me devuelve clave: valor

#forma alternativa de recorrer diccionarios

for clave,valor in colores.items():
	print(clave,valor) #me entrega clave valor


#pop

aux = colores.pop('negro',"no se ha encontrado")

print(aux)

print(colores)

#borrar un diccionario
colores.clear()