from tkinter import *

root = Tk()


root.title("Curso udemy")
root.resizable(1,1) # largo, ancho > para no redimencionar la ventana principal
#root.icobitmap() para incorporar un icono a la ventana

frame = Frame(root, width=480, height=320)
frame.pack(fill='both', expand=1) #nos deja las mismas dimensiones del frame como del root
frame.config(cursor="pirate")
frame.config(bg="lightblue")
frame.config(bd=25)
frame.config(relief="sunken")


root.config(cursor="arrow")
root.config(bg="blue") #color de fondo
root.config(bd=15) #dejamos un borde, en pixeles
root.config(relief="ridge") #para darle relieve a los bordes


#siempre a bajo de todo
root.mainloop() #me crea la ventana principal

