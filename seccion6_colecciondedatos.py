#tuplas! -> aceptan identacion y slicing ->tuplas no se pueden modificiar, no se puede usar append para agregar 

tupla =(100,"hola",[1,2,3],-50)

print(tupla) 
print(tupla[0])

print(tupla[2:])

print (len(tupla)) #recordatorio : len cuenta los elementos de una coleccion

#podemos usar .index() para buscar un elemento y saber su posicion 

print(tupla.index("hola"))
 # si no se le pasa un valor de la tupla tira error

cantidad = tupla.count(100)
print("la cantidad de veces que se repite el elemento 100 es: ",cantidad)

#.count(nos cuenta la cantidadn de elementos repetidos que están en la tupla)



#---------------------------------------------------------------------------------------#

#conjuntos!! 

# colecciones de elementos desordenados unicos, se utilizan para eliminar elementos duplicados, soportan operaciones matematicas


conjunto = set()

conjunto1 = {1,2,3}
#el orden no se conserva al momento de añadir elementos
#metodo mas utilizado es .add() ->añadir

conjunto1.add(4)

print("este es un conjunto  ",conjunto1)

conjunto1.add(0)
conjunto1.add("hola")
print("este es el nuevo conjunto al agregar un elemento",conjunto1)

#la caracteristica principal de un conjunto es que no pueden haber elementos duplicados en el 
#tambien se puede verificar si un elemento se encuentra en el conjunto

grupo = {"pedro","juan", "diego"}

respuesta = input("ingrese un nombre para verificar si se encuetra en el conjunto: ")
respuesta in grupo 
if True:
	print ("se encuentra en el grupo")
else:
	print("no se encuentra en el grupo")


#tambien se pueden pasar listas a conjuntos usando el metodo set()

lista=[1,1,1,2,2,3]

print("lista de elementos -> ",lista)

conjuntoresultante= set(lista)

print("la lista se ha transformado en un conjunto -> ",conjuntoresultante)

#con list() se transforma un conjunto en una lista de nuevo 
#para eliminar los elementos repetidos de una lista, podemos hacer lo siguiente
lista1 = [1,1,1,1,1,1,2,2,2,2,2,3,3,3,3]
lista1 = list(set(lista1)) #lo que estoy haciendo es transformar la lista en un conjunto,
#este elimina los elementos repetidos y luego tranformo el conjunto en una lista nuevamente 
print ("lista -> ", lista1)


#------------------------------------------------------------------------------------------------------#

#Dicccionarios -> clave valor ->arreglos asosiativos
#son colecciones desordenadas 
print("DICCIONARIOS")

diccionariosvacio= {}
type(diccionariosvacio)
			#clave       valor
colores = {'amarillo': 'yellow','azul': 'blue', 'verde': 'greee'}

colores['azul']
colores['amarillo'] = "white" #estoy redefiniendo un valor del diccionario

print(colores)

#borrar

del(colores['amarillo'])
print(colores)

edades = {'hector': 27, 'juan':25,'maria':34}

#suma en asignacion para aumentar la edad

edades['juan'] +=2 
print("la nueva edad de juan es: ",edades['juan'])

suma_edades = edades['juan'] + edades['maria']

print("la suma de las edades de juan y maria es -> ", suma_edades)


#---- recorrer un diccionario con un for----#
#acceder a las palabras claves -> con unfor accedemos a esto
for edad in edades:
	print(edad)

#muestra los valores 
for clave in edades:
	print(edades[clave])

#mostrar ambas
#con metodo interno items 
for clave,valor in edades.items():
	print(clave,valor)

#parecido al enumerate de las listas 

#mezcla de diccionarios y listas/ diccionarios en listas

personajes = []

p= {'nombre': 'gandalf', 'clase': 'mago', 'raza': 'humano'}

personajes.append(p)

print (personajes)

p2= {'nombre': 'logolas', 'clase': 'arquero', 'raza': 'elfo'}

p3= {'nombre': 'gimli', 'clase': 'guerrero', 'raza': 'enano'}

personajes.append(p2)
personajes.append(p3) #append es de las listas
print (personajes)


for personaje in personajes:
	print(personaje['nombre'],personaje['clase'],personaje['raza'])


#---------PILAS Y COLAS------------#

#PILAS (LIFO ULTIMO EN ENTRAR PRIMERO EN SALIR) ESTRUCTURAS DE DATOS QUE POSEEN DOS ACCIONES PUSH Y POP 
#(APILAR Y DESAPILAR ) EJEMPLO: LIBROS, EL ULTIMO ES EL QUE QUEDA ENCIMA DE LA PILA Y ES EL PRIMERO EN SALIR

#las pilas son listas
pila= [3,4,5]

pila.append(6)

pila.append(7)

print ("la pila creada tiene los siguientes elementos -> ", pila)

#para sacar elementos se utiliza el metodo .pop()

pila.pop()

print("la pila es -> ",pila)



#-----COLAS-----#

#COLAS ->FIFO PRIMERO IN ENTRAR PRIMERO EN SALIR 

from collections import deque

cola = deque()

cola = deque(['alfonso','gloria','teresa'])

cola.append('maria')
cola.append('lucho')

print(cola)

#borrar -> metodo viene en la coleccion deque

cola.popleft()

print(cola)

#para no perder un elemento de la una coleccion al momento de borrarlo
#se aconseja guardarlo en una variable, asi quedará el registro 

#p = cola.popleft() 

#estamos guardando en la variable p el elemento que se va a elminar 
# de la cola para no perderlo 




