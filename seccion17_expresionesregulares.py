

#patrones de busqueda definidos con una sintaxis formal, busquedas simples y avanzadas en cadenas de texto
#busquedas sencillas

#sintaxis intermedias 

import re

textos = "en esta cadena se encuentra una palabra magica"
#re.search -> busca un patron dentro de una cadena

palabra = "magica"
encontrado = re.search(palabra,textos) #devuelve un objeto de tipo sre.match y si no esta devuelve none

if encontrado is not None:
	print("Se ha encontrado ",palabra," en el texto")
else:
	print("Erro! ",palabra," no está en el texto")

#metodos

print(encontrado.start()) #posicion dentro de la cadena donde parte la palabra	
print(encontrado.end()) #posicion final donde termina la palabra

print(encontrado.span())#tupla con incio y fin


print(encontrado.string)

cadena = "hola mundo"
print(re.match("hola",cadena)) #nos indica si la cadena empiexa por la palabra que le pasas como parametro
#nos devuelve un objeto match si es verdad si no nos devuelve none

cadena2 = "vamos a dividir esta-cadena"

print(re.split("-",cadena2)) # devuelve una lista con los las divisiones que realizas con el split

cad = "hola amigo hola hola hola"
print(re.sub("amigo","amiga",cad)) #sub nos permite cambiar una palabra dentro de una cadena
cad = re.sub("amigo","amiga",cad) #sobre escribiendo
print(cad)

print(re.findall("hola",cad)) #devuelve una lista con los hola que pilla en la cadena
print(len(re.findall("hola",cad))) # nos cuenta la cantidad de veces que se repite la palabra

print(re.findall("hola|amigo",cad))

#expresiones regulares con patrones de sintaxis repetida


texto = "hla hola hoola hoooola hoooooooooola hooooooooooola"
def buscar(patrones,texto):
	for patron in patrones:
		print(re.findall(patron,texto))

patrones = ["hla","ho+la","ho*la","ho?la","ho{2}la"]
#meta caracter * nos indica que va a buscar 0 o todas las cantidades de o que tenga el texto
print(buscar(patrones,texto))

#tambien existe el meta caracter + que nos indica 1 o todas los caracteres que se repiten
#el metacaracter ? nos indica 1 o 0 caracteres de los patrones
#la sintaxis {n} nos deja indicarle que nos busque la cantidad de veces que queremos que se repita 
#la letra tambien puede ser un rango de la forma {n,m } ho{2,10}la



#conjunto de caracteres

"""
Rangos [ - ]
Otra característica que hace ultra potentes los grupos, es la capacidad de definir rangos. Ejemplos de rangos:
[A-Z]: Cualquier carácter alfabético en mayúscula (no especial ni número)
[a-z]: Cualquier carácter alfabético en minúscula (no especial ni número)
[A-Za-z]: Cualquier carácter alfabético en minúscula o mayúscula (no especial ni número)
[A-z]: Cualquier carácter alfabético en minúscula o mayúscula (no especial ni número)
[0-9]: Cualquier carácter numérico (no especial ni alfabético)
[a-zA-Z0-9]: Cualquier carácter alfanumérico (no especial)


"""

texto = "hola h0la Hola mola m0la M0la"

patrones = ['h[a-z]la', 'h[0-9]la', '[A-z]{4}', '[A-Z][A-z0-9]{3}'] 
print(buscar(patrones, texto))


texto = "Este curso de Python se publicó en el año 2016"

patrones = [r'\d+', r'\D+', r'\s', r'\S+', r'\w+', r'\W+'] 
print(buscar(patrones, texto))