

def saludar():
	print("hola te estoy saludando desde un módulo externo llamado ejemplodemodulo.py")

def sumar(a,b):
	suma = a+b
	print("la suma es -> ",suma)

class Auto():
	"""docstring for ClassName"""
	def __init__(self, marca, color, anio):
		super(Auto, self).__init__()
		self.marca = marca
		self.color = color
		self.anio = anio

	def pintar(self,colornuevo):
		self.color = colornuevo
		print("se ha cambiado color a ", self.color)


		