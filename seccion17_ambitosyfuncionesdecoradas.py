

#AMBITOS Y FUNCIONES DECORADAS

"""
No cabe duda de que Python es un lenguaje flexible, y cuando trabajamos con funciones no es 
una excepción.
En Python, dentro de una función podemos definir otras funciones. Con la peculiaridad de que 
el ámbito de 
estas funciones se encuentre únicamente dentro de la función padre. 
Vamos a trabajar los ámbitos un poco más en profundidad:

"""

lista = [1,2,3] #ambito global del script

def hola():
    numero = 50
    def bienvenido(): # bienvenido es una funcion de ambito local de la funcion hola
        return "Hola!"
    #print(locals())#para mostrar el contenido en el ambito local de la funcion hola
    #print(globals()) #para mostrar las variables globales
    return bienvenido

#print(bienvenido())

#print(hola())

#print(globals().keys())


#funciones como variables 

#print(hola()()) -> una forma de acceder a la funcion bienvenido dentro de hola


#otra forma de acceder a bienvenido() es asignando la funcion hola() a una variable
# y despues imprimirla como funcion >< 

bienvenido = hola()

print(bienvenido())


#en python se pueden enviar como argumento de una funcion  otra funcion
#este proceso se llama decorador 

def chao():
	return "aadioooos!"


def test(funcion):
	print(funcion)


test(chao())



# funciones decoradoras 






def monitorizar(funcion): #funcion decoradora

	def decorar():
		print("\t se está a punto de ejecutar la funcion: ", funcion.__name__)

		print(funcion())

		print("\t se ha terminado de ejecutar la funcion: ",funcion.__name__)


	return decorar

@monitorizar # estoy llamando a la funcion decoradora 
def holi():
	return "holaaa!!!"

@monitorizar
def adios():
	return "Adioosoososos!"

#monitorizar(holi)()
#monitorizar(adios)()

holi()

adios()


#Los decoradores nos permiten configurar un codigo externo para complementar la ejecucion de otra funcion distinta



#no funciona si las funciones tienen argumentos 


#cuando se declara una funcion se llaman parametros, cuando se llama una funcion se llaman argumentos


#funciones decoradoras con argumentos
def monitorizar_args(funcion): #funcion decoradora

	def decorar(*args,**kwargs):
		print("\t se está a punto de ejecutar la funcion: ", funcion.__name__)

		print(funcion(*args,*kwargs))

		print("\t se ha terminado de ejecutar la funcion: ",funcion.__name__)


	return decorar



@monitorizar_args # estoy llamando a la funcion decoradora 
def holis(nombre):
	print("hola {}!! ".format(nombre))

@monitorizar_args
def adio(nombre):
	print("adios!! {}!! ".format(nombre))



holis("juan")

adio("juan")