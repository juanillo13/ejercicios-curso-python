"""
) Localiza el error en el siguiente bloque de código. Crea una excepción para evitar que el programa 
se bloquee y además explica en un mensaje al usuario la causa y/o solución:
In [13]:
# Completa el ejercicio aquí
resultado = 10/0
"""
#SOLUCION
try:
	resultado = 10/0
except ZeroDivisionError:
	print("no se puede dividir por 0!!!")

"""
2) Localiza el error en el siguiente bloque de código. Crea una excepción para evitar que el programa 
se bloquee y además explica en un mensaje al usuario la causa y/o solución:
In [14]:
# Completa el ejercicio aquí
lista = [1, 2, 3, 4, 5]
lista[10]
"""

#SOLUCION

try:
	lista=[1,2,3,4,5]
	lista[10]
except IndexError:
	print("el valor solicitado está fuera del rango de la lista")


"""
Realiza una función llamada agregar_una_vez() que reciba una lista y un elemento. La función debe 
añadir el elemento al final de la lista con la condición de no repetir ningún elemento. Además si 
este elemento ya se encuentra en la lista se debe invocar un error de tipo ValueError que debes 
capturar y mostrar este mensaje en su lugar:
Error: Imposible añadir elementos duplicados => [elemento].
Prueba de agregar los elementos 10, -2, "Hola" a la lista de elementos con la función una vez la 
has creado y luego muestra su contenido.
Nota: Puedes utilizar la sintaxis: elemento in lista
"""

elementos = [1,2,3]

def agregar_una_vez(lista,elemento):
	try:
		if elemento in lista: #si el elemento está en la lista invoco un valueError que lo tomo en el except y mando los print por pantalla
			raise ValueError
		else:
			lista.append(elemento)
	except ValueError:
		print("la lista se compone de los siguientes elementos ->", lista)
		print("error! es imposible agregar elementos duplicados -> ",elemento)
	print(lista)

agregar_una_vez(elementos,1)
agregar_una_vez(elementos,10)
agregar_una_vez(elementos,-2)
agregar_una_vez(elementos,-2)


