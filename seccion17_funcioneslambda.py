

"""

funciones lambda nos permite crear funciones anonimas, sin nombre, que 
nos deja crear funciones de una sola expresión en vez de un bloque de acciones

sirven para realizar funciones simples

"""


def doblar(numero):
	resultado = numero * 2

	return resultado

print(doblar(5))


def dobla(numero):
	return numero * 2

print(dobla(5))


def d (n) : return n*2
print(d(5))

#todas estas expresiones de funciones son equivalentes, solo cambia la sintaxis
#una funcion lambda intenta replicar la última expresión, pero sin definirla propiamente tal

#funcion lambda


print(lambda num : num * 2) #me imprime un objeto identificando que es una funcion lambda y una dirección de memoria

#para utilizarla se debe guardar en una variable

doblarr = lambda num : num * 2


# se llama como una funcion

print(doblarr(3))


impar = lambda num : num % 2 != 0 

numero = int(input("ingrese un numero: "))

print(impar(numero))

if impar(numero) is True:
	print("vaya! {} es un numero impar".format(numero))
else:
	print("no es impar")



